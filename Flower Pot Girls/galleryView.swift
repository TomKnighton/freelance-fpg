//
//  galleryView.swift
//  Flower Pot Girls
//
//  Created by Tom Knighton on 28/10/2018.
//  Copyright © 2018 Tom Knighton. All rights reserved.
//

import UIKit
import ChameleonFramework
class galleryView: UITableViewController {

    
    @IBOutlet weak var image44: UIImageView!
    @IBOutlet weak var image43: UIImageView!
    @IBOutlet weak var image42: UIImageView!
    @IBOutlet weak var image41: UIImageView!
    @IBOutlet weak var image40: UIImageView!
    @IBOutlet weak var image39: UIImageView!
    @IBOutlet weak var image38: UIImageView!
    @IBOutlet weak var image37: UIImageView!
    @IBOutlet weak var image36: UIImageView!
    @IBOutlet weak var image35: UIImageView!
    @IBOutlet weak var image34: UIImageView!
    @IBOutlet weak var image33: UIImageView!
    @IBOutlet weak var image32: UIImageView!
    @IBOutlet weak var image31: UIImageView!
    @IBOutlet weak var image30: UIImageView!
    @IBOutlet weak var image29: UIImageView!
    @IBOutlet weak var image28: UIImageView!
    @IBOutlet weak var image27: UIImageView!
    @IBOutlet weak var image26: UIImageView!
    @IBOutlet weak var image25: UIImageView!
    @IBOutlet weak var image24: UIImageView!
    @IBOutlet weak var image23: UIImageView!
    @IBOutlet weak var image22: UIImageView!
    @IBOutlet weak var image21: UIImageView!
    @IBOutlet weak var image20: UIImageView!
    @IBOutlet weak var image19: UIImageView!
    @IBOutlet weak var image18: UIImageView!
    @IBOutlet weak var image17: UIImageView!
    @IBOutlet weak var image16: UIImageView!
    @IBOutlet weak var image15: UIImageView!
    @IBOutlet weak var image14: UIImageView!
    @IBOutlet weak var image13: UIImageView!
    @IBOutlet weak var image12: UIImageView!
    @IBOutlet weak var image11: UIImageView!
    @IBOutlet weak var image10: UIImageView!
    @IBOutlet weak var image9: UIImageView!
    @IBOutlet weak var image8: UIImageView!
    @IBOutlet weak var image7: UIImageView!
    @IBOutlet weak var image6: UIImageView!
    @IBOutlet weak var image5: UIImageView!
    @IBOutlet weak var image4: UIImageView!
    @IBOutlet weak var image3: UIImageView!
    @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var image1: UIImageView!
    
    
    @IBOutlet weak var holder1: UIView!
    @IBOutlet weak var holder3: UIView!
    @IBOutlet weak var holder2: UIView!
    @IBOutlet weak var holder4: UIView!
    @IBOutlet weak var holder5: UIView!
    @IBOutlet weak var holder6: UIView!
    @IBOutlet weak var holder7: UIView!
    @IBOutlet weak var holder8: UIView!
    @IBOutlet weak var holder9: UIView!
    @IBOutlet weak var holder10: UIView!
    @IBOutlet weak var holder11: UIView!
    @IBOutlet weak var holder12: UIView!
    @IBOutlet weak var holder13: UIView!
    @IBOutlet weak var holder14: UIView!
    @IBOutlet weak var holder15: UIView!
    @IBOutlet weak var holder16: UIView!
    @IBOutlet weak var holder17: UIView!
    @IBOutlet weak var holder18: UIView!
    @IBOutlet weak var holder19: UIView!
    @IBOutlet weak var holder20: UIView!
    @IBOutlet weak var holder21: UIView!
    @IBOutlet weak var holder22: UIView!
    @IBOutlet weak var holder23: UIView!
    @IBOutlet weak var holder24: UIView!
    @IBOutlet weak var holder25: UIView!
    @IBOutlet weak var holder26: UIView!
    @IBOutlet weak var holder27: UIView!
    @IBOutlet weak var holder28: UIView!
    @IBOutlet weak var holder29: UIView!
    @IBOutlet weak var holder30: UIView!
    @IBOutlet weak var holder31: UIView!
    @IBOutlet weak var holder32: UIView!
    @IBOutlet weak var holder33: UIView!
    @IBOutlet weak var holder34: UIView!
    @IBOutlet weak var holder35: UIView!
    @IBOutlet weak var holder36: UIView!
    @IBOutlet weak var holder37: UIView!
    @IBOutlet weak var holder38: UIView!
    @IBOutlet weak var holder39: UIView!
    @IBOutlet weak var holder40: UIView!
    @IBOutlet weak var holder41: UIView!
    @IBOutlet weak var holder42: UIView!
    @IBOutlet weak var holder43: UIView!
    @IBOutlet weak var holder44: UIView!
    
    
    static var imageTapped = UIImage()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.holder1.layer.cornerRadius = 20
        self.holder1.layer.masksToBounds = true
        self.holder2.layer.cornerRadius = 20
        self.holder2.layer.masksToBounds = true
        self.holder3.layer.cornerRadius = 20
        self.holder3.layer.masksToBounds = true
        self.holder4.layer.cornerRadius = 20
        self.holder4.layer.masksToBounds = true
        self.holder5.layer.cornerRadius = 20
        self.holder5.layer.masksToBounds = true
        self.holder6.layer.cornerRadius = 20
        self.holder6.layer.masksToBounds = true
        self.holder7.layer.cornerRadius = 20
        self.holder7.layer.masksToBounds = true
        self.holder8.layer.cornerRadius = 20
        self.holder8.layer.masksToBounds = true
        self.holder9.layer.cornerRadius = 20
        self.holder9.layer.masksToBounds = true
        self.holder10.layer.cornerRadius = 20
        self.holder10.layer.masksToBounds = true
        self.holder11.layer.cornerRadius = 20
        self.holder11.layer.masksToBounds = true
        self.holder12.layer.cornerRadius = 20
        self.holder12.layer.masksToBounds = true
        self.holder13.layer.cornerRadius = 20
        self.holder13.layer.masksToBounds = true
        self.holder14.layer.cornerRadius = 20
        self.holder14.layer.masksToBounds = true
        self.holder15.layer.cornerRadius = 20
        self.holder15.layer.masksToBounds = true
        self.holder16.layer.cornerRadius = 20
        self.holder16.layer.masksToBounds = true
        self.holder17.layer.cornerRadius = 20
        self.holder17.layer.masksToBounds = true
        self.holder18.layer.cornerRadius = 20
        self.holder18.layer.masksToBounds = true
        self.holder19.layer.cornerRadius = 20
        self.holder19.layer.masksToBounds = true
        self.holder20.layer.cornerRadius = 20
        self.holder20.layer.masksToBounds = true
        self.holder21.layer.cornerRadius = 20
        self.holder21.layer.masksToBounds = true
        self.holder22.layer.cornerRadius = 20
        self.holder22.layer.masksToBounds = true
        self.holder23.layer.cornerRadius = 20
        self.holder23.layer.masksToBounds = true
        self.holder24.layer.cornerRadius = 20
        self.holder24.layer.masksToBounds = true
        self.holder25.layer.cornerRadius = 20
        self.holder25.layer.masksToBounds = true
        self.holder26.layer.cornerRadius = 20
        self.holder26.layer.masksToBounds = true
        self.holder27.layer.cornerRadius = 20
        self.holder27.layer.masksToBounds = true
        self.holder28.layer.cornerRadius = 20
        self.holder28.layer.masksToBounds = true
        self.holder29.layer.cornerRadius = 20
        self.holder29.layer.masksToBounds = true
        self.holder30.layer.cornerRadius = 20
        self.holder30.layer.masksToBounds = true
        self.holder31.layer.cornerRadius = 20
        self.holder31.layer.masksToBounds = true
        self.holder32.layer.cornerRadius = 20
        self.holder32.layer.masksToBounds = true
        self.holder33.layer.cornerRadius = 20
        self.holder33.layer.masksToBounds = true
        self.holder34.layer.cornerRadius = 20
        self.holder34.layer.masksToBounds = true
        self.holder35.layer.cornerRadius = 20
        self.holder35.layer.masksToBounds = true
        self.holder36.layer.cornerRadius = 20
        self.holder36.layer.masksToBounds = true
        self.holder37.layer.cornerRadius = 20
        self.holder37.layer.masksToBounds = true
        self.holder38.layer.cornerRadius = 20
        self.holder38.layer.masksToBounds = true
        self.holder39.layer.cornerRadius = 20
        self.holder39.layer.masksToBounds = true
        self.holder40.layer.cornerRadius = 20
        self.holder40.layer.masksToBounds = true
        self.holder41.layer.cornerRadius = 20
        self.holder41.layer.masksToBounds = true
        self.holder42.layer.cornerRadius = 20
        self.holder42.layer.masksToBounds = true
        self.holder43.layer.cornerRadius = 20
        self.holder43.layer.masksToBounds = true
        self.holder44.layer.cornerRadius = 20
        self.holder44.layer.masksToBounds = true
        
        
        
        self.holder1.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder1.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder2.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder2.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder3.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder3.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder4.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder4.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder5.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder5.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder6.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder6.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder7.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder7.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder8.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder8.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder9.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder9.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder10.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder10.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder11.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder11.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder12.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder12.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder13.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder13.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder14.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder14.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder15.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder15.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder16.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder16.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder17.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder17.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder18.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder18.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder19.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder19.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder20.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder20.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder21.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder21.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder22.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder22.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder23.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder23.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder24.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder24.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder25.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder25.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder26.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder26.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder27.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder27.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder28.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder28.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder29.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder29.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder30.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder30.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder31.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder31.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder32.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder32.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder33.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder33.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder34.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder34.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder35.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder35.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder36.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder36.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder37.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder37.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder38.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder38.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder39.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder39.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder40.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder40.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder41.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder41.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder42.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder42.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder43.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder43.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.holder44.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.holder44.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])

    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "galleryToFull" {
            let vc = segue.destination as! popupView
            vc.image = galleryView.imageTapped
        }
    }
   

}
