//
//  mainPage.swift
//  Flower Pot Girls
//
//  Created by Tom Knighton on 27/10/2018.
//  Copyright © 2018 Tom Knighton. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import ChameleonFramework

class mainPage: UITableViewController {

    @IBOutlet var table: UITableView!
    @IBOutlet weak var instHolder: UIView!
    @IBOutlet weak var galleryHolder: UIView!
    @IBOutlet weak var moreHolder: UIView!
    @IBOutlet weak var subscribeBtn: UIButton!
    @IBOutlet weak var contactHolder: UIView!
    
    @IBOutlet weak var prev1: UIImageView!
    @IBOutlet weak var prev2: UIImageView!
    
    @IBOutlet weak var step1: UIImageView!
    @IBOutlet weak var step2: UIImageView!
    @IBOutlet weak var step3: UIImageView!
    @IBOutlet weak var step4: UIImageView!
    @IBOutlet weak var play1: UIImageView!
    @IBOutlet weak var play2: UIImageView!
    @IBOutlet weak var play3: UIImageView!
    @IBOutlet weak var play4: UIImageView!
    
    
    @IBOutlet weak var mainThumb: UIImageView!
    @IBOutlet weak var mainPlay: UIImageView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.instHolder.layer.cornerRadius = 20
        self.galleryHolder.layer.cornerRadius = 20
        self.moreHolder.layer.cornerRadius = 20
        self.contactHolder.layer.cornerRadius = 20
        self.instHolder.layer.masksToBounds = true
        self.galleryHolder.layer.masksToBounds = true
        self.moreHolder.layer.masksToBounds = true
        self.contactHolder.layer.masksToBounds = true
        
        self.mainThumb.layer.cornerRadius = 20
        self.mainThumb.layer.masksToBounds = true
        
        self.instHolder.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.instHolder.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.galleryHolder.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.galleryHolder.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.moreHolder.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.moreHolder.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
         self.contactHolder.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.contactHolder.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
    }
    
    
    @IBAction func playMain(_ sender: Any) {
        guard let path = Bundle.main.path(forResource: "mainvid", ofType:"mp4") else {
            debugPrint("mainvid.mp4 not found")
            return
        }
        let player = AVPlayer(url: URL(fileURLWithPath: path))
        let playerController = AVPlayerViewController()
        playerController.player = player
        present(playerController, animated: true) {
            player.play()
        }
    }
    
    
    @IBAction func vid1(_ sender: Any) {
        guard let path = Bundle.main.path(forResource: "vid1", ofType:"mov") else {
            debugPrint("vid1.mov not found")
            return
        }
        let player = AVPlayer(url: URL(fileURLWithPath: path))
        let playerController = AVPlayerViewController()
        playerController.player = player
        present(playerController, animated: true) {
            player.play()
        }
    }
    
    @IBAction func vid2(_ sender: Any) {
        guard let path = Bundle.main.path(forResource: "vid2", ofType:"mov") else {
            debugPrint("vid2.mov not found")
            return
        }
        let player = AVPlayer(url: URL(fileURLWithPath: path))
        let playerController = AVPlayerViewController()
        playerController.player = player
        present(playerController, animated: true) {
            player.play()
        }
    }
    
    @IBAction func vid3(_ sender: Any) {
        guard let path = Bundle.main.path(forResource: "vid3", ofType:"mov") else {
            debugPrint("vid3.mov not found")
            return
        }
        let player = AVPlayer(url: URL(fileURLWithPath: path))
        let playerController = AVPlayerViewController()
        playerController.player = player
        present(playerController, animated: true) {
            player.play()
        }
    }
    
    @IBAction func vid4(_ sender: Any) {
        guard let path = Bundle.main.path(forResource: "vid4", ofType:"mov") else {
            debugPrint("vid4.mov not found")
            return
        }
        let player = AVPlayer(url: URL(fileURLWithPath: path))
        let playerController = AVPlayerViewController()
        playerController.player = player
        present(playerController, animated: true) {
            player.play()
        }
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        
        coordinator.animate(alongsideTransition: { (context) in
            
        }) { (completion) in
            self.instHolder.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.instHolder.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
            self.galleryHolder.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.galleryHolder.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
            self.moreHolder.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.moreHolder.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        }
        super.viewWillTransition(to: size, with: coordinator)
        
    }
    override func viewDidLayoutSubviews() {
        self.instHolder.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.instHolder.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.galleryHolder.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.galleryHolder.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
        self.moreHolder.backgroundColor = GradientColor(gradientStyle: .leftToRight, frame: self.moreHolder.frame, colors: [UIColor(hexString: "#DA4453"), UIColor(hexString: "#89216B")])
    }
    
    
    
    
    
    var imageTapped = UIImage()
    
    @IBAction func imageTapped(_ sender: UITapGestureRecognizer) {
        let imageView = sender.view as! UIImageView
        self.imageTapped = imageView.image!
        self.performSegue(withIdentifier: "mainToPreview", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "mainToPreview" {
            let vc = segue.destination as! popupView
            vc.image = imageTapped
        }
    }
    
    
    
}
