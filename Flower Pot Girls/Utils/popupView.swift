//
//  popupView.swift
//  Flower Pot Girls
//
//  Created by Tom Knighton on 27/10/2018.
//  Copyright © 2018 Tom Knighton. All rights reserved.
//

import UIKit

class popupView: UIViewController {

    @IBOutlet weak var imageP: UIImageView!
    @IBOutlet weak var closeBtn: UIButton!
    
    var image = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.closeBtn.layer.cornerRadius = 10
        self.closeBtn.layer.masksToBounds = true
    }

    override func viewDidAppear(_ animated: Bool) {
        self.imageP.image = self.image
    }
    
    @IBAction func dismissed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)
    
    @IBAction func swipeGet(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.view?.window)
        
        if sender.state == UIGestureRecognizerState.began {
            initialTouchPoint = touchPoint
        } else if sender.state == UIGestureRecognizerState.changed {
            if touchPoint.y - initialTouchPoint.y > 0 {
                self.view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        } else if sender.state == UIGestureRecognizerState.ended || sender.state == UIGestureRecognizerState.cancelled {
            if touchPoint.y - initialTouchPoint.y > 100 {
                self.dismiss(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }
    }
    
}
