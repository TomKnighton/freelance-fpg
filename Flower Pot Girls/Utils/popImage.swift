//
//  popImage.swift
//  Flower Pot Girls
//
//  Created by Tom Knighton on 27/10/2018.
//  Copyright © 2018 Tom Knighton. All rights reserved.
//

import UIKit

class popImage: UIImageView {

    let tapRec = UITapGestureRecognizer()
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(popImage.showFullScreen))
        self.addGestureRecognizer(tapGesture)
        self.isUserInteractionEnabled = true
        
    }
    
    
    @objc func showFullScreen() {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController.childViewControllers[0]
            }
            
            topController = topController as! galleryView
            galleryView.imageTapped = self.image!
            topController.performSegue(withIdentifier: "galleryToFull", sender: self)
        }
    }
    

    
}
